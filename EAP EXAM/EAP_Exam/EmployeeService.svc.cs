﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EAP_Exam
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmployeeService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmployeeService.svc or EmployeeService.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeService : IEmployeeService
    {
        EmployeeContextDataContext db = new EmployeeContextDataContext();

        public string AddEmployee(Employee emp)
        {
            try
            {
                db.Employees.InsertOnSubmit(emp);
                db.SubmitChanges();
                return "Add Employee Success!";
            }
            catch
            {
                return "Add Employee Failed!";
            }
        }

        public List<Employee> GetEmployeeList()
        {
            try
            {
                return (from emp in db.Employees select emp).ToList();

            }
            catch
            {
                return null;
            }
        }

        public List<Employee> GetEmployeesByDepartment(string dpmName)
        {
            try
            {
                return (from emp in db.Employees
                        where emp.Department.Contains(dpmName)
                        select emp).ToList();
            }
            catch
            {
                return null;
            }
        }
    }
}
