﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using AssignmentASP.Models;

namespace AssignmentASP.DAL
{
    public class AssignmentContext : DbContext
    {
        public AssignmentContext() : base("AssignmentContext")
        {

        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Food> Foods { get; set; }
        public DbSet<DefaultMenu> DefaultMenus { get; set; }
        public DbSet<DefaultMenuDetail> DefaultMenuDetails { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
    }
}