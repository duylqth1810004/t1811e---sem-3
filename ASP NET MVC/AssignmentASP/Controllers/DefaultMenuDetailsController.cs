﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AssignmentASP.DAL;
using AssignmentASP.Models;

namespace AssignmentASP.Controllers
{
    public class DefaultMenuDetailsController : Controller
    {
        private AssignmentContext db = new AssignmentContext();

        // GET: DefaultMenuDetails
        public ActionResult Index()
        {
            var defaultMenuDetails = db.DefaultMenuDetails.Include(d => d.Food);
            return View(defaultMenuDetails.ToList());
        }

        // GET: DefaultMenuDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DefaultMenuDetail defaultMenuDetail = db.DefaultMenuDetails.Find(id);
            if (defaultMenuDetail == null)
            {
                return HttpNotFound();
            }
            return View(defaultMenuDetail);
        }

        // GET: DefaultMenuDetails/Create
        public ActionResult Create()
        {
            ViewBag.FoodID = new SelectList(db.Foods, "FoodID", "Name");
            return View();
        }

        // POST: DefaultMenuDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DefaultMenuDetailID,FoodID,DefaultMenuID,Quantity")] DefaultMenuDetail defaultMenuDetail)
        {
            if (ModelState.IsValid)
            {
                db.DefaultMenuDetails.Add(defaultMenuDetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FoodID = new SelectList(db.Foods, "FoodID", "Name", defaultMenuDetail.FoodID);
            return View(defaultMenuDetail);
        }

        // GET: DefaultMenuDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DefaultMenuDetail defaultMenuDetail = db.DefaultMenuDetails.Find(id);
            if (defaultMenuDetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.FoodID = new SelectList(db.Foods, "FoodID", "Name", defaultMenuDetail.FoodID);
            return View(defaultMenuDetail);
        }

        // POST: DefaultMenuDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DefaultMenuDetailID,FoodID,DefaultMenuID,Quantity")] DefaultMenuDetail defaultMenuDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(defaultMenuDetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FoodID = new SelectList(db.Foods, "FoodID", "Name", defaultMenuDetail.FoodID);
            return View(defaultMenuDetail);
        }

        // GET: DefaultMenuDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DefaultMenuDetail defaultMenuDetail = db.DefaultMenuDetails.Find(id);
            if (defaultMenuDetail == null)
            {
                return HttpNotFound();
            }
            return View(defaultMenuDetail);
        }

        // POST: DefaultMenuDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DefaultMenuDetail defaultMenuDetail = db.DefaultMenuDetails.Find(id);
            db.DefaultMenuDetails.Remove(defaultMenuDetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
