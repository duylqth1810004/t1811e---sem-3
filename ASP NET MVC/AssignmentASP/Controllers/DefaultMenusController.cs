﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AssignmentASP.DAL;
using AssignmentASP.Models;

namespace AssignmentASP.Controllers
{
    public class DefaultMenusController : Controller
    {
        private AssignmentContext db = new AssignmentContext();

        // GET: DefaultMenus
        public ActionResult Index()
        {
            return View(db.DefaultMenus.ToList());
        }

        // GET: DefaultMenus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DefaultMenu defaultMenu = db.DefaultMenus.Find(id);
            if (defaultMenu == null)
            {
                return HttpNotFound();
            }
            return View(defaultMenu);
        }

        // GET: DefaultMenus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DefaultMenus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DefaultMenuDetailID,Name,Discount,Description")] DefaultMenu defaultMenu)
        {
            if (ModelState.IsValid)
            {
                db.DefaultMenus.Add(defaultMenu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(defaultMenu);
        }

        // GET: DefaultMenus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DefaultMenu defaultMenu = db.DefaultMenus.Find(id);
            if (defaultMenu == null)
            {
                return HttpNotFound();
            }
            return View(defaultMenu);
        }

        // POST: DefaultMenus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DefaultMenuDetailID,Name,Discount,Description")] DefaultMenu defaultMenu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(defaultMenu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(defaultMenu);
        }

        // GET: DefaultMenus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DefaultMenu defaultMenu = db.DefaultMenus.Find(id);
            if (defaultMenu == null)
            {
                return HttpNotFound();
            }
            return View(defaultMenu);
        }

        // POST: DefaultMenus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DefaultMenu defaultMenu = db.DefaultMenus.Find(id);
            db.DefaultMenus.Remove(defaultMenu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
