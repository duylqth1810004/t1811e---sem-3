namespace AssignmentASP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigrate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerID = c.Int(nullable: false, identity: true),
                        PhoneNumber = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Username = c.String(nullable: false),
                        Password = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.CustomerID);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderID = c.Int(nullable: false, identity: true),
                        StartMealTime = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        PeopleAmount = c.Int(nullable: false),
                        Discount = c.Decimal(precision: 18, scale: 2),
                        TableOrdered = c.Int(nullable: false),
                        PaymentMethod = c.Int(nullable: false),
                        TotalCost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Note = c.String(),
                        CustomerID = c.Int(nullable: false),
                        EmployeeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderID)
                .ForeignKey("dbo.Customers", t => t.CustomerID, cascadeDelete: true)
                .ForeignKey("dbo.Employees", t => t.EmployeeID, cascadeDelete: true)
                .Index(t => t.CustomerID)
                .Index(t => t.EmployeeID);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        EmployeeID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        SystemLevel = c.Int(nullable: false),
                        Dob = c.DateTime(nullable: false),
                        Username = c.String(nullable: false),
                        Password = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.EmployeeID);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        OrderDetailID = c.Int(nullable: false, identity: true),
                        OrderID = c.Int(nullable: false),
                        FoodID = c.Int(),
                        DefaultMenuID = c.Int(),
                        Amount = c.Int(nullable: false),
                        DefaultMenu_DefaultMenuDetailID = c.Int(),
                    })
                .PrimaryKey(t => t.OrderDetailID)
                .ForeignKey("dbo.Foods", t => t.FoodID)
                .ForeignKey("dbo.DefaultMenus", t => t.DefaultMenu_DefaultMenuDetailID)
                .ForeignKey("dbo.Orders", t => t.OrderID, cascadeDelete: true)
                .Index(t => t.OrderID)
                .Index(t => t.FoodID)
                .Index(t => t.DefaultMenu_DefaultMenuDetailID);
            
            CreateTable(
                "dbo.DefaultMenus",
                c => new
                    {
                        DefaultMenuDetailID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Discount = c.Decimal(precision: 18, scale: 2),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.DefaultMenuDetailID);
            
            CreateTable(
                "dbo.DefaultMenuDetails",
                c => new
                    {
                        DefaultMenuDetailID = c.Int(nullable: false, identity: true),
                        FoodID = c.Int(),
                        DefaultMenuID = c.Int(),
                        Quantity = c.Int(nullable: false),
                        DefaultMenu_DefaultMenuDetailID = c.Int(),
                    })
                .PrimaryKey(t => t.DefaultMenuDetailID)
                .ForeignKey("dbo.DefaultMenus", t => t.DefaultMenu_DefaultMenuDetailID)
                .ForeignKey("dbo.Foods", t => t.FoodID)
                .Index(t => t.FoodID)
                .Index(t => t.DefaultMenu_DefaultMenuDetailID);
            
            CreateTable(
                "dbo.Foods",
                c => new
                    {
                        FoodID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Cost = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.FoodID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderDetails", "OrderID", "dbo.Orders");
            DropForeignKey("dbo.OrderDetails", "DefaultMenu_DefaultMenuDetailID", "dbo.DefaultMenus");
            DropForeignKey("dbo.OrderDetails", "FoodID", "dbo.Foods");
            DropForeignKey("dbo.DefaultMenuDetails", "FoodID", "dbo.Foods");
            DropForeignKey("dbo.DefaultMenuDetails", "DefaultMenu_DefaultMenuDetailID", "dbo.DefaultMenus");
            DropForeignKey("dbo.Orders", "EmployeeID", "dbo.Employees");
            DropForeignKey("dbo.Orders", "CustomerID", "dbo.Customers");
            DropIndex("dbo.DefaultMenuDetails", new[] { "DefaultMenu_DefaultMenuDetailID" });
            DropIndex("dbo.DefaultMenuDetails", new[] { "FoodID" });
            DropIndex("dbo.OrderDetails", new[] { "DefaultMenu_DefaultMenuDetailID" });
            DropIndex("dbo.OrderDetails", new[] { "FoodID" });
            DropIndex("dbo.OrderDetails", new[] { "OrderID" });
            DropIndex("dbo.Orders", new[] { "EmployeeID" });
            DropIndex("dbo.Orders", new[] { "CustomerID" });
            DropTable("dbo.Foods");
            DropTable("dbo.DefaultMenuDetails");
            DropTable("dbo.DefaultMenus");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.Employees");
            DropTable("dbo.Orders");
            DropTable("dbo.Customers");
        }
    }
}
