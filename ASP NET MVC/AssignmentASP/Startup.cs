﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AssignmentASP.Startup))]
namespace AssignmentASP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
