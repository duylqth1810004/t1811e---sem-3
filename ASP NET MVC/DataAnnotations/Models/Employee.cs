﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DataAnnotations.Models
{
    public class Employee
    {
        [Key]
        [ScaffoldColumn(false)]//Cái này ẩn đi ô nhập cho trường Id trên UI (User Interface - Giao diện người dùng), trong trường hợp
        //này thì mặc định không có vì Id trong CSDL được đặt tự tăng!
        public int Id { get; set; }


        [Required(ErrorMessage="Vui lòng nhập Tên!")]
        [DisplayName("Employee Name")]//Cái này hiển thị Employee Name thay cho Name trong View, nhìn sẽ thuận mắt hơn Name (lấy từ CSDL)
        [StringLength(15,MinimumLength = 5, ErrorMessage ="Tên phải ít hơn 15 và nhiều hơn 5 ký tự!")]
        public string Name { get; set; }


        [Compare("Name")]//Cái này thường dùng trong trường hợp ô password với re-enter password, nếu 2 cái không giống nhau
        //thì sẽ báo lỗi, ở đây là nếu "Name" với "Address" không giống nhau thì nó sẽ báo lỗi!
        public string Address { get; set; }


        [Range(10000,500000, ErrorMessage ="Bát buộc giá trị nhập vào Salary phải lớn hơn 10000 và nhỏ hơn 50000")]
        [RegularExpression(@"^[0 - 9](\.[0 - 9] +)?$")]//Cái này là sử dụng regex(Biểu thức chính quy) để ép người dùng chỉ có thể nhập số thập
        //phân lớn hơn 0
        public decimal Salary { get; set; }


        [DataType(DataType.EmailAddress, ErrorMessage ="Bắt buộc phải nhập vào Email đúng định dạng email xyz@abc")]
        public string Email { get; set; }
    }
}