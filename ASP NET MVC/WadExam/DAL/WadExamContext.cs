﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WadExam.Models;

namespace WadExam.DAL
{
    public class WadExamContext : DbContext
    {
        public WadExamContext() : base("WadExamContext")
        {

        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}