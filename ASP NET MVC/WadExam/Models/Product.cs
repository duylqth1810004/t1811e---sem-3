﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace WadExam.Models
{
    public class Product
    {
        [Key][DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductId { get; set; }

        [Required][Display(Name = "Product Name")]
        public string ProductName { get; set; }

        [Required][Display(Name = "Supplier ID")]
        public int SupplierId { get; set; }
        public int CategoryId { get; set; }

        [Display(Name = "Quantity Per Unit")]
        public int QuantityPerUnit { get; set; }

        [Display(Name = "Unit Price")]
        public decimal UnitPrice { get; set; }

        [Display(Name = "Units In Stock")]
        public int UnitsInStock { get; set; }

        [Display(Name = "Units In Order")]
        public int UnitsInOrder { get; set; }

        [Display(Name = "Reorder Level")]
        public int ReorderLevel { get; set; }
        public string Discontinued { get; set; }
        public virtual Category Category{ get; set; }
    }
}